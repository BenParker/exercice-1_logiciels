<?php
require_once './Connexion.classe.php';

class Select
{
	public function selectCountProduit()
	{
		$monPDO = new Connexion();
		$connexion = $monPDO->getPDO();
		$monStatement = $connexion->prepare("select count(numeroProduit) from materiaux");
		$monStatement->execute();
		$liste = $monStatement->fetch()[0];
		
		return $liste;
	}

	public function selectCountQuantiteZero()
	{
		$monPDO = new Connexion();
		$connexion = $monPDO->getPDO();
		$monStatement = $connexion->prepare("select count(numeroProduit) from materiaux where quantite=0");
		$monStatement->execute();
		$liste = $monStatement->fetch()[0];
		
		return $liste;
	}
}

