<?php

require_once "./Select.classe.php";

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    //private $info;
    private $selection;

    // public function testInfo()
    // {
    //     //Hotfix à faire ici.
    //     //Modifié.
    //     $this->info = new Info("LoL");
    //     $this->assertEquals("LoL",  $this->info->getInfo());
    // }

    public function testSelectProduit()
    {
        $this->selection = new Select();
        $this->assertEquals(5,  $this->selection->selectCountProduit());
    }

    public function testSelectQuantiteZero()
    {
        $this->selection = new Select();
        $this->assertEquals(2,  $this->selection->selectCountQuantiteZero());
    }

}

