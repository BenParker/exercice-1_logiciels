<?php
require 'config.inc.php';
/**
 * Description of Info
 *
 * @author claudeboutet
 */
class Info {
    private $info;
    
    public function __construct($param) {
        $this->info = $param;
    }
    
    public function getInfo() {
        if (!empty($this->info)){
            return $this->info;
        }
        
    }
}
