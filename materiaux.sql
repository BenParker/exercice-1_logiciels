
 create database materiaux;
 use materiaux;
 CREATE TABLE materiaux (
  numeroProduit int NOT NULL primary key,
  nomComplet varchar(100) NOT NULL,
  quantite varchar(100) NOT NULL,
  seuilMinQuantite varchar(100) NOT NULL
);

--
-- Contenu de la table `materiaux`
--

Insert into materiaux values (0, "Marteau", 25, 5);
Insert into materiaux values (1, "Casque", 0, 12);
Insert into materiaux values (2, "Tournevis", 3, 15);
Insert into materiaux values (3, "Perceuse", 11, 4);
Insert into materiaux values (4, "Étau", 0, 3);


CREATE USER 'php'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT ON materiaux.materiaux to 'php'@'localhost';
FLUSH PRIVILEGES;